#ifndef FILTER_H
#define FILTER_H

#include "book_of_records.h"

note** filter(note* array[], int size, bool (*check)(note* element), int& result_size);

bool check_by_wind_orientation(note* element);

bool check_by_wind_speed(note* element);

note** s_sort(note* array[], int size, int (*check)(note* first, note* second));

note** merge_sort(note* array[], int size, int (*check)(note* first, note* second));

int crit_1(note* first, note* second); 

int crit_2(note* first, note* second);


#endif