#include "filter.h"
#include"file_reader.cpp"
#include <cstring>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>


using namespace std;

note** filter(note* array[], int size, bool (*check)(note* element), int& result_size)
{
    note** result = new note * [size];
    result_size = 0;
    for (int i = 0; i < size; i++)
    {
        if (check(array[i]))
        {
            result[result_size++] = array[i];
        }
    }
    return result;
}

//�������� �����������
bool check_by_wind_orientation(note* element)
{
    return  strcmp(element->orientation, "West") == 0 ||
            strcmp(element->orientation, "NorthWest") == 0 ||
            strcmp(element->orientation, "North") == 0;
}

//�������� �������� ������ 5
bool check_by_wind_speed(note* element)
{
    return element->speed >= 5;
}


int crit_1(note* first, note* second)
{
    if (first->speed == second->speed)
    {
        return 0;
    }
    else if (first->speed < second->speed)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

int crit_2(note* first, note* second)
{
    if (first->orientation[0] == second->orientation[0])
    {
        if (first->months.month == second->months.month)
        {
            if (first->days.day == second->days.day)
            {
                return 0;
            }
            else if (first->days.day < second->days.day)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
        else if (first->months.month < second->months.month)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    else if (first->orientation[0] < second->orientation[0])
    {
        return -1;
    }
    else
    {
        return 1;
    }
}

//����e���� ����������
note** s_sort(note* array[], int size, int (*check)(note* first, note* second))
{
    note* temp = new note[size];
    int i;
    int left = 1;
    int right = size - 1;
    while (left <= right)
    {
        for (i = right; i >= left; i--)
        {
            if (check(array[i - 1], array[i]) == 1)
            {
                temp = array[i];
                array[i] = array[i - 1];
                array[i - 1] = temp;
            }
        }
        left++;
        for (i = left; i <= right; i++)
        {
            if (check(array[i - 1], array[i]) == 1)
            {
                temp = array[i];
                array[i] = array[i - 1];
                array[i - 1] = temp;
            }
        }
        right--;
    }
    return array;
}

note** merge_sort(note* array[], int size, int (*check)(note* first, note* second))
{
    int mid = size / 2;
    if (size % 2 == 1)
    {
        mid++;
    }
    
    int h = 1;

    note** c = new note*[size];
    int step;
    while (h < size)
    {
        step = h;
        int i = 0;
        int j = mid;
        int k = 0;

        while (step <= mid)
        {
            if (array[i] < array[j])
            {
                c[k] = array[i];
                i++;
                k++;
            }
            else
            {
                c[k] = array[j];
                j++; 
                k++;
            }
            while (i < step)
            {
                c[k] = array[i];
                i++;
                k++;
            }
            while ((j < (mid + step)) && (j < size))
            {
                c[k] = array[j];
                j++;
                k++;
            }
            step += h;
        }
        h *= 2;

        for (i = 0; i < size; i++)
        {
            array[i] = c[i];
        }
    }


    return array;
}
  