#include <iostream>
#include <iomanip>
#include "book_of_records.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

using namespace std;

void output(note* records)
{
    /********** ����� ���� **********/
    //����� ���
    cout << records->days.day << ' ';
    //����� ������
    cout << records->months.month << ' ';
    /********** ����� ���������� ����� **********/
    //����� ����������� �����
    cout << setw(11) << records->orientation << ' ';
    //����� �������� �����
    cout << records->speed << ' ';
    cout << '\n';
}

int main()
{

    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �6. ���� ������\n";
    cout << "�����: ��������� ��������\n";
    cout << "������: 12\n\n";
    note* records[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        /**** ����� �������� ������ ****/
        read_file("data.txt", records, size);
        cout << "***** ���� ������ *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(records[i]);
        }

        bool (*check_function)(note*) = NULL;
        note** (*s_method)(note * array[], int size, int (*check)(note * first, note * second)) = 0;
        int (*s_criteria)(note * first, note * second) = 0;

        /******** ����� �������� ***********/
        cout << "\n ��������� ��������:\n";
        cout << "1) ������� ���, � ������� ��� ����� � ����� �� ����������� West, NorthWest ��� North\n";
        cout << "2) ������� ���, � ������� ��� ����� ������ 5 �/�\n";
        cout << "3) ������������� ������\n";
        cout << "������� ����� ��������: ";
        int item;
        cin >> item;
        cout << '\n';
        int criteria = 0, method = 0;
        switch (item)
        {
        case 1:
            check_function = check_by_wind_orientation; //�������� ����������� 
            cout << "***** �� ����������� ����� *****\n";
            break;

        case 2:
            check_function = check_by_wind_speed; //�������� �������� �����     
            cout << "***** �� �������� ����� *****\n";
            break;

        case 3:
            cout << "������ ����������: \n";
            cout << "1) ��������� ���������� \n";
            cout << "2) ���������� �������� \n";
            cout << "������� ����� ������: ";
            cin >> method;
            cout << '\n';
            switch (method)
            {
            case 1:
                s_method = s_sort;
                break;
            case 2:
                s_method = merge_sort;
                break;
            default:
                throw " ������ ����� ";
            }

            cout << "�������� ����������: \n";
            cout << "1) �� �������� �������� �����\n";
            cout << "2) �� ����������� ����������� �����\n";
            cout << "������� ����� ��������: ";
            cin >> criteria;
            switch (criteria)
            {
            case 1:
                s_criteria = crit_1;
                break;
            case 2:
                s_criteria = crit_2;
                break;
            default:
                throw " ������ ����� ";
            }
            break;

        default:
            throw " ������ ����� ";
        }

        //���������� ������
       
        if (check_function)
        {
            int new_size;
            note** filtered = filter(records, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }

        else
        {
            s_method(records, size, s_criteria);
            for (int i = 0; i < size; i++)
            {
                output(records[i]);
            }
        }

        for (int i = 0; i < size; i++)
        {
            delete records[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }

    return 0;
}
