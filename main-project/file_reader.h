#ifndef FILE_READER_H
#define FILE_READER_H

#include "book_of_records.h"
#include "file_reader.h"

void read_file(const char* file_name, note* array[], int& size);

#endif
