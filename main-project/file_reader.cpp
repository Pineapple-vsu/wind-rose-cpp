#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

/*date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    return result;
}
*/
void read_file(const char* file_name, note* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            note* item = new note;
            file >> item->days.day;
            file >> item->months.month;
            file >> item->orientation;
            file >> item->speed;
            file.read(tmp_buffer, 1);
            array[size++] = item;
            
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}