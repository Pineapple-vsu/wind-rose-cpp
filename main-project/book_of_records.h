#ifndef BOOK_OF_RECORDS_H
#define BOOK_OF_RECORDS_H

#include "constants.h"

struct date
{
    int day;
    int month;
};

struct note
{
    date days;
    date months;
    char orientation[MAX_STRING_SIZE];
    double speed;
};

#endif
